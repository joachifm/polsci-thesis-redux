#+LATEX_CLASS: thesis

#+OPTIONS: ':t *:t -:t ::t <:nil H:3 \n:nil ^:t arch:nil
#+OPTIONS: author:t c:nil creator:comment d:(not LOGBOOK) date:t e:t
#+OPTIONS: email:nil f:t inline:t num:t p:nil pri:nil stat:nil tags:nil
#+OPTIONS: tasks:t tex:t timestamp:t toc:nil todo:nil |:t
#+OPTIONS: texht:t
#+CREATOR: Emacs 24.3.1 (Org mode 8.0.2)
#+EXCLUDE_TAGS: noexport
#+SELECT_TAGS: export
#+LANGUAGE: en
