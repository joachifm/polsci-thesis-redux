#!/bin/bash

make master.tex >/dev/null

WP=354
WC=$(texcount master.tex 2>&1 | awk -F ': ' '/Words in text:/ { print $2 }')
PC=$((WC / WP))
GIT_VERSION=$(git describe --always --abbrev=6)

echo "Words in text: $WC (~= $PC pages @ $WP w/p) ($GIT_VERSION)" | tee -a .lastcount
uniq .lastcount > .lastcount.tmp
mv .lastcount.tmp .lastcount
