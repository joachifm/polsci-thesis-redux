;;;; init.el -- minimal emacs configuration

;;; Org mode initialization

(let ((org-path "~/local/src/org-mode.git"))
  (add-to-list 'load-path (concat org-path "/lisp"))
  (add-to-list 'load-path (concat org-path "/contrib/lisp")))

;;; LaTeX export

;; Better handling of single space
(require 'org-special-blocks)
(setq org-entities-user '(("space" "\\ " nil " " " " " " " ")))

;; Tweak latex export back-end
(require 'ox-latex)

(setq org-latex-remove-logfiles nil)
(setq org-latex-pdf-process '("latexmk -pdf %f"))

;; Override default because it causes breakage for me ...
(setq org-latex-default-packages-alist
      '(
        ("AUTO" "inputenc" t)
        ("T1" "fontenc" t)
        ("" "fixltx2e" nil)
        ("" "graphicx" t)
        ("" "longtable" nil)
        ("" "wrapfig" t)
        ("" "float" nil)
        ("" "textcomp" t)
        ("" "hyperref" nil)
        ("" "amsmath" t)
        ("" "amssymb" t)
        ("" "latexsym" t)
        "\\tolerance=1000")
      )

;; Custom class for thesis
(add-to-list 'org-latex-classes
             '("thesis"
               "\\documentclass[a4paper,12pt]{report}
                \\usepackage{mystyle}
                [PACKAGES]
                [EXTRA]"
               ("\\chapter{%s}" . "\\chapter*{%s}")
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")
               )
             )
